﻿using UnityEngine;

/** Object to call when you want to play a sound
*/
public sealed class AudioManager : MonoBehaviour
{
    #region Variables
    public AudioSource[] effectSource;
    private int _selected = 0;
    public AudioSource musicSource;
    public static AudioManager instance;            
    public float lowPitchRange = .95f;
    public float highPitchRange = 1.05f;
    #endregion

    #region Unity Methods

    public void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

        DontDestroyOnLoad(gameObject);
    }

    #endregion

    /** Plays the passed audioclip
     *  @param clip Sound to play
    */
    public void PlaySingle(AudioClip clip)
    {
        _selected++; _selected %= effectSource.Length;
        effectSource[_selected].clip = clip;
        effectSource[_selected].Play();
    }

    /** Chooses a random soundclip from all the passed audioclips
     *  @param clips Clips to choose between
    */
    public void RandomizeSfx(params AudioClip[] clips)
    {
        int randomIndex = Random.Range(0, clips.Length);
        float randomPitch = Random.Range(lowPitchRange, highPitchRange);

        _selected++; _selected %= effectSource.Length;
        effectSource[_selected].pitch = randomPitch;
        effectSource[_selected].clip = clips[randomIndex];
        effectSource[_selected].Play();
    }
}