﻿using UnityEngine;
using UnityEngine.UI;

public class UI : MonoBehaviour
{
    public static UI instance;
    [SerializeField] private Text _countdownText;
    [SerializeField] private Text _jumpText;

    [Header("Sprites")]
    [SerializeField] private Texture2D _progressSprite;
    [SerializeField] private Texture2D _noProgressSprite;
    [SerializeField] private Texture2D _healthSprite;

    private Player _player;
    private bool showStats = true;

    public void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
    }

    public void SetPlayer(Player player) { _player = player; }

    public void SetCountdownText(string text)
    {
        _countdownText.text = text;
    }

    public void SetJumpCountText(string text)
    {
        _jumpText.text = text;
    }

    public void OnGUI()
    {
        if (!showStats) return;
        DrawOrbs();
        DrawHealth();
    }

    /** Draws the HUD element that showcases the picked up orbs
     */
    private void DrawOrbs()
    {
        for (int i = 0; i < _player.orbCap; i++)
        {
            GUI.DrawTexture(new Rect(32 + 40 * i, 32, 32, 32), (i < _player.orbCount) ? _progressSprite : _noProgressSprite);
        }
    }

    /** Draws the HUD element that showcases health
     */
    private void DrawHealth()
    {
        for (int i = 0; i < _player.Health; i++)
        {
            GUI.DrawTexture(new Rect(32 + 40 * i, 32 + 40, 32, 32), _healthSprite);
        }
    }

    public void GameOver()
    {
        _countdownText.text = "Game Over!";
        showStats = false;
    }

}
