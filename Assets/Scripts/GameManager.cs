﻿using System.Collections;
using UnityEngine;

/** Enemy and orb spawning
*/
public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    [Header("Game Objects")]
    public GameObject obstacle;
    public Color obstacleColor;
    public GameObject orb;
    public Color orbColor;

    [Header("Level Selection")]
    public AudioClip selectSound;

    private readonly string[] _startText = { "Wave starting!", "3", "2", "1", "Start!"};
    private Texture2D _level;
    private Texture2D[] _allLevels;

    private int LevelsUnlocked{ get { return PlayerPrefs.GetInt("LevelsUnlocked", 1); } set { PlayerPrefs.SetInt("LevelsUnlocked", value); }}

    public void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

        _allLevels = Resources.LoadAll<Texture2D>("Levels");
    }

    public IEnumerator Start()
    {
        yield return SelectLevel();
        yield return WaveStarting();
        yield return Wave();
        yield return WaveEnding();

        StartCoroutine(Start());
    }

    private IEnumerator SelectLevel()
    {
        while (_level == null)
        {
            yield return null;
        }
    }

    private IEnumerator WaveStarting()
    {
        foreach (string s in _startText)
        {
            UI.instance.SetCountdownText(s);
            yield return new WaitForSeconds(1f);
        }
        UI.instance.SetCountdownText("");
    }

    private IEnumerator Wave()
    {
        int x = 0;
        while (++x < _level.width)
        {
            for (int y = 0; y < _level.height; y++)
            {
                Color pixel = _level.GetPixel(x, y);
           
                if(pixel == obstacleColor) Instantiate(obstacle, new Vector3(8f, 0.25f + 0.5f * y, 0f), Quaternion.identity);
                if(pixel == orbColor) Instantiate(orb, new Vector3(8f, 0.25f + 0.5f * y, 0f), Quaternion.identity);
            }
            
            yield return new WaitForSeconds(1f);
        }

        yield return new WaitForSeconds(2f);
        
    }

    private IEnumerator WaveEnding()
    {
        UI.instance.SetCountdownText("Wave over!!!");
        yield return new WaitForSeconds(3f);
        UI.instance.SetCountdownText("");
        _level = null;
        LevelsUnlocked++;
    }

    public void GameOver()
    {
        UI.instance.GameOver();
    }

    public void OnGUI()
    {
        if (_level != null) return;
        for (int i = 0; i < _allLevels.Length; i++)
        {
            if (i + 1 > LevelsUnlocked) continue;
            if (GUI.Button(new Rect(Screen.width/2 + 120 * ((i % 4) - 2), Screen.height/2 + 120 * ((i / 4) - 1), 80, 50), "Level: " + (1 + i)))
            {
                _level = _allLevels[i];
                AudioManager.instance.PlaySingle(selectSound);
            }
        }
    }

}
