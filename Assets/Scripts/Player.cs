﻿using UnityEngine;

/** Player logic
*/
public class Player : MonoBehaviour
{
    private int _jumpCount;
    public float jumpPower = 1f;

    /** Health of the player, this is also saved
    */
    public int Health
    {
        get { return PlayerPrefs.GetInt("Health", 3); }
        set { PlayerPrefs.SetInt("Health", value); }
    }

    [HideInInspector] public int orbCount;
    public readonly int orbCap = 10;

    #region Effects
    public AudioClip jumpSound;
    public ParticleSystem jumpEffect;
    public AudioClip landSound;
    public ParticleSystem landEffect;
    #endregion

    private bool _isGrounded;
    private Rigidbody _rigidBody;

    public void Start()
    {
        _rigidBody = GetComponent<Rigidbody>();
        _jumpCount = PlayerPrefs.GetInt("JumpCount", 0);
        Health = PlayerPrefs.GetInt("Health", 3);
        orbCount = PlayerPrefs.GetInt("OrbCount", 0);

        if (!UI.instance) return;
        UI.instance.SetJumpCountText(_jumpCount.ToString());
        UI.instance.SetPlayer(this);
    }

    public void Update()
    {
        if (!_rigidBody) return;
        if (Input.GetKeyDown(KeyCode.Space))Jump(); 
    }

    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Floor")
        {
            _isGrounded = true;
            AudioManager.instance.PlaySingle(landSound);
            landEffect.Play();
        }
    }

    public void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.tag == "Floor") _isGrounded = false;
    }

    /** Checks if the player can jump and jumps
    */
    private void Jump()
    {
        if (!_isGrounded) return;
        _rigidBody.AddForce(Vector3.up * jumpPower * 100f);
        AudioManager.instance.PlaySingle(jumpSound);
        jumpEffect.Play();
        PlayerPrefs.SetInt("JumpCount", ++_jumpCount);

        if(UI.instance) UI.instance.SetJumpCountText(_jumpCount.ToString());
    }

    /**Add to the orb counter
     * @param value Amount to add
    */
    public void AddOrb(int value = 0)
    {
        orbCount += value;
        if (orbCount == orbCap)
        {
            Health++;
            orbCount = 0;
        }
        PlayerPrefs.SetInt("OrbCount", orbCount);
    }

    /** Resets health and orbcount to default
    */
    public void ResetStats()
    {
        PlayerPrefs.DeleteKey("OrbCount");
        PlayerPrefs.DeleteKey("Health");
    }

    /** Deals damage to health and checks if player should die
     * @param value Amount health should be decreased
    */
    public void Damage(int value = 1)
    {
        Health -= value;
        if(Health <= 0) Die();
    }

    /** Everything that should happen when the player dies
    */
    public void Die()
    {
        ResetStats();
        GameManager.instance.GameOver();
        Destroy(gameObject);
    }
}
