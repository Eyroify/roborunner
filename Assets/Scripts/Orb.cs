﻿using UnityEngine;

public class Orb : MonoBehaviour
{
    public float speed = 4f;
    public int orbsDrop = 1;
    public AudioClip orbSound;
    public GameObject particleExplosion;

    public void Reset(Vector3 start)
    {
        transform.position = start;
    }

    public void Update()
    {
        transform.Translate(Vector3.left * speed * Time.deltaTime);
        if (transform.position.x < -5) Destroy(gameObject);
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            AudioManager.instance.PlaySingle(orbSound);
            other.GetComponent<Player>().AddOrb(orbsDrop);
            GameObject explosion = Instantiate(particleExplosion, transform.position, transform.rotation);
            Destroy(explosion, 2f);
            Destroy(gameObject);
        }
    }
}
