﻿using UnityEngine;

/** Obstacle
*/
public class Obstacle : MonoBehaviour
{
    public float speed = 4f;
    public int damage = 1;
    public AudioClip hitSound;
    public GameObject particleExplosion;

    public void Reset(Vector3 start)
    {
        transform.position = start;
    }

    public void Update()
    {
		transform.Translate(Vector3.left * speed * Time.deltaTime);

        if(transform.position.x < -5) Destroy(gameObject);
	}

    public void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            AudioManager.instance.PlaySingle(hitSound);
            other.GetComponent<Player>().Damage(damage);
            GameObject explosion = Instantiate(particleExplosion, transform.position, transform.rotation);
            Destroy(explosion, 2f);
            Destroy(gameObject);
        }
    }
}
